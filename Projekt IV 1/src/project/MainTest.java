import java.awt.GridLayout;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

private JPanel buildTopShelf() {
		JPanel mediaPanel = new JPanel();
		int nrOfMedias = myMediaHandler.mediaList.size();
		if(nrOfMedias < 6) {
		mediaPanel.setOpaque(false);
		mediaPanel.setLayout(new GridLayout(1,6,22,0));
		mediaPanel.setBounds(90, 14, 1020, 210);
		
		java.net.URL where;
		try {
			where = new URL("https://dl.dropboxusercontent.com/u/16670644/Projekt/TempPic.png");
			ImageIcon image = new ImageIcon(where);			
			for(int i = nrOfMedias; i < 6; i++) {
				JLabel imageLabel = new JLabel(image);
				mediaPanel.add(imageLabel);
			}
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			System.out.println("Error loading TempPic that shows allowed media spots");
			e.printStackTrace();
		}
		return mediaPanel;
	}
		else {
			return mediaPanel;
		}
	}
	
	private JPanel buildMiddleShelf() {
		JPanel mediaPanel = new JPanel();
		int nrOfMedias = myMediaHandler.mediaList.size();
		if(nrOfMedias > 6 && nrOfMedias < 12) {
		mediaPanel.setOpaque(false);
		mediaPanel.setLayout(new GridLayout(1,6,22,0));
		mediaPanel.setBounds(90, 275, 1020, 210);
			
		java.net.URL where;
		try {
			where = new URL("https://dl.dropboxusercontent.com/u/16670644/Projekt/TempPic.png");
			ImageIcon image = new ImageIcon(where);
			for(int i = 0; i < 6; i++) {
				JLabel imageLabel = new JLabel(image);
				mediaPanel.add(imageLabel);
			}
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			System.out.println("Error loading TempPic that shows allowed media spots");
			e.printStackTrace();
		}
		return mediaPanel;
	}
		else {
			return mediaPanel;
		}
	}
	
	private JPanel buildLowerShelf() {
		JPanel mediaPanel = new JPanel();
		int nrOfMedias = myMediaHandler.mediaList.size();
		if(nrOfMedias > 12 && nrOfMedias < 18) {
		mediaPanel.setOpaque(false);
		mediaPanel.setLayout(new GridLayout(1,6,22,0));
		mediaPanel.setBounds(90, 550, 1020, 210);
		//mediaPanel.setBackground(Color.BLACK);
		
		java.net.URL where;
		try {
			where = new URL("https://dl.dropboxusercontent.com/u/16670644/Projekt/TempPic.png");
			ImageIcon image = new ImageIcon(where);
			for(int i = 0; i < 6; i++) {
				JLabel imageLabel = new JLabel(image);
				mediaPanel.add(imageLabel);
			}
			
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			System.out.println("Error loading TempPic that shows allowed media spots");
			e.printStackTrace();
		}
		return mediaPanel;
	}
		else {
			return mediaPanel;
		}
	}
	
	
	
	
	
	
	/*------------------------------------------*/
	
	
	private JPanel buildShelfs() {
		JPanel mediaPanel = new JPanel();
		int nrOfMedias = myMediaHandler.mediaList.size();
		mediaPanel.setOpaque(false);
		mediaPanel.setLayout(new GridLayout(3,6,22,30));
		mediaPanel.setBounds(90, 25, 1020, 745);
	
				java.net.URL where;
				for(int i = 0; i < nrOfMedias; i++) {
					String temp = myMediaHandler.mediaList.get(i).getImagePath();
					try {
						where = new URL(temp);
						ImageIcon image = new ImageIcon(where);
						JLabel imageLabel = new JLabel(image);
						mediaPanel.add(imageLabel);
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						System.out.println("Error loading user media picture");
						e.printStackTrace();
					}
				}
	
			java.net.URL where1;
			try {
				for(int i = nrOfMedias; i < 18; i++) {
					where1 = new URL("https://dl.dropboxusercontent.com/u/16670644/Projekt/TempPic.png");
					ImageIcon image1 = new ImageIcon(where1);			
					JLabel imageLabel1 = new JLabel(image1);
					mediaPanel.add(imageLabel1);
				}
				
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				System.out.println("Error loading TempPic that shows allowed media spots");
				e.printStackTrace();
			}

			return mediaPanel;
	}
	