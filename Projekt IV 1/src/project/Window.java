package project;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import javax.swing.JLabel;
import javax.swing.ImageIcon;

public class Window extends JFrame {
	
	private static final long serialVersionUID = 1L;
	
	private MediaHandler myMediaHandler = new MediaHandler(this);
	private JPanel mediaPanel;
	private MediaInfo myMediaInfo;
	
	class MenuActionListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String buttonText = e.getActionCommand();
			switch(buttonText) {
			case "Add Movie":
				AddMovieWindow addMovieGUI = new AddMovieWindow(myMediaHandler);
				addMovieGUI.setVisible(true);
				break;
			case "Add TV-Show":
				AddTVShowWindow addTVShowGUI = new AddTVShowWindow(myMediaHandler);
				addTVShowGUI.setVisible(true);
				break;
			case "Save on file":
			myMediaHandler.saveOnFile();
			break;
			}
		}
	}
	
	// System reads from file upon start. Allowing medias to be added on start. 
	public Window() {
		addComponents();
		configurFrame();
		addMenu();
		mediaPanel.repaint();
		myMediaHandler.readFromFile();
	}
	
	private void addMenu() {
		JMenuBar bar = new JMenuBar();
		JMenu menu = new JMenu("Menu");
		bar.add(menu);
		
		JMenuItem item = new JMenuItem("Add Movie");
		item.addActionListener(new MenuActionListener());
		menu.add(item);
		
		item = new JMenuItem("Add TV-Show");
		item.addActionListener(new MenuActionListener());
		menu.add(item);
		
		item = new JMenuItem("Save on file");
		item.addActionListener(new MenuActionListener());
		menu.add(item);
		
		this.setJMenuBar(bar);
	}
	
	private void configurFrame() {
		this.setSize(1205, 850);
		this.setTitle("Video Library");
		this.setLocationRelativeTo(null);
		this.setResizable(false);
		this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		this.setIconImage(new ImageIcon("icon.png").getImage());
		this.add(new JLabel(new ImageIcon("BackGround.png")));
	}
	
	private void addComponents() {
		JPanel shelfs = buildShelfs();
		this.add(shelfs);
	}

	// Building the "shelfs" that contain movie and TV-show images. A MouseListener is also adding allowing the clicked image to return its properties. 
	private JPanel buildShelfs() {
		mediaPanel = new JPanel();
		mediaPanel.setOpaque(false);
		mediaPanel.setLayout(new GridLayout(3,6,22,30));
		mediaPanel.setBounds(90, 25, 1020, 745);
		mediaPanel.addMouseListener(new MouseListener() {				
				@Override
				public void mouseClicked(MouseEvent arg0) {		
	
					try {
						String getPath = null;
						try {
							// Gets the URL added to the mediaPanel. (needed in function bellow).
							getPath = ((JLabel) mediaPanel.getComponentAt(arg0.getX(), arg0.getY())).getIcon().toString();
						} catch(Exception e) {
							return;
						}
						
						System.out.println(getPath); //Image URL from the GridLayout!
						// Compares the URL from the mediaPanel with the URL,s in the ArrayList allowing me to get the position of the media from the ArrayList.
						int result = myMediaHandler.getPosOfMedia(getPath);
						
						// Creates a new mediaInfo class and sends information from the media being cliced on. 
						myMediaInfo = new MediaInfo(myMediaHandler.getMedia(result));
						myMediaInfo.setVisible(true);
					} catch(UnsupportedOperationException e) {
						e.printStackTrace();
					}
				}

				@Override
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				@Override
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				@Override
				public void mousePressed(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				@Override
				public void mouseReleased(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
			});
			return mediaPanel;
	}
	
	
	/*private void addMedia(int index) {
		String urlString = "https://dl.dropboxusercontent.com/u/16670644/Projekt/TempPic.png";
		
		int nrOfMedias = myMediaHandler.mediaList.size();
		if(index < nrOfMedias) {
			urlString = myMediaHandler.mediaList.get(index).getImagePath();
		}
		addMedia(urlString);
	}*/
	
	// Adds a image to the mediaPanel. The image URL is taken from the List.
	void addMedia(String urlString) {
		try
        {
            java.net.URL where = new URL(urlString);
            ImageIcon image = new ImageIcon(where);
            JLabel imageLabel = new JLabel(image);
            mediaPanel.add(imageLabel);
            mediaPanel.invalidate();
            mediaPanel.validate();
        }
        catch (MalformedURLException e)
        {
            System.out.println("Error loading user media picture from "+urlString);
            e.printStackTrace();
        }
	}
	
	public static void main(String[] arg) {
		Window mainWindowGUI = new Window();
		mainWindowGUI.setVisible(true);
	}	
}