package project;

public abstract class Media {

	private String title;
	private Double playTime;
	private int year;
	private boolean seen;
	private String director;
	private String mediaPath;
	private String imagePath;
	public enum rating{unrated,one,two,three,four,five};
	private rating myRating;
	
	public Media(String title, Double playTime, int year, boolean seen,
			String directory, String mediaPath, String imagePath, rating myRating) {
		super();
		this.title = title;
		this.playTime = playTime;
		this.year = year;
		this.seen = seen;
		this.director = directory;
		this.mediaPath = mediaPath;
		this.imagePath = imagePath;
		this.myRating = myRating;
	}
	
	public Media() {
		super();
		this.title = "unknown";
		this.playTime = 00.00;
		this.year = 0000;
		this.seen = false;
		this.director = "unknown";
		this.mediaPath = "unknown";
		this.imagePath = "unknown";
		this.myRating = rating.unrated;
	}
	
	public abstract String toStringSpec();	

	@Override
	public String toString() {
		return title + "\r\n" + playTime + "\r\n" + year + "\r\n" +
				seen + "\r\n" + director+ "\r\n" + mediaPath + "\r\n" + imagePath +
				"\r\n" + myRating + "\r\n" + toStringSpec();
	}
	
	public abstract String infoStringSpec();
	
	// Additional toString allowing special information to be displayed about the media. 
	public String infoString() {
		String seenOrNot = null;
		if(seen == true) {
			seenOrNot = "Already viewed";
		} else {
			seenOrNot = "Not viewed";
		}
		
		return "Title: " + title + "\r\n" + "Play Time: " + playTime + "s" + "\r\n" + "Year: " + year + "\r\n" 
		+ "Seen: " + seenOrNot + "\r\n" + "Directory: " + director+ "\r\n" + infoStringSpec();
	}

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public Double getPlayTime() {
		return playTime;
	}
	
	public void setPlayTime(Double playTime) {
		this.playTime = playTime;
	}
	
	public int getYear() {
		return year;
	}
	
	public void setYear(int year) {
		this.year = year;
	}
	
	public boolean isSeen() {
		return seen;
	}
	
	public void setSeen(boolean seen) {
		this.seen = seen;
	}
	
	public String getDirector() {
		return director;
	}
	
	public void setDirector(String directory) {
		this.director = directory;
	}

	public String getMediaPath() {
		return mediaPath;
	}

	public void setMediaPath(String mediaPath) {
		this.mediaPath = mediaPath;
	}

	public rating getMyRating() {
		return myRating;
	}

	public void setMyRating(rating myRating) {
		this.myRating = myRating;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
}