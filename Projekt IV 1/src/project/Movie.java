package project;

public class Movie extends Media {

	private String quality;
	private boolean subtitles;
	private String language;
	private String writer;
	
	public Movie(String title, Double playTime, int year, boolean seen,
			String directory, String mediaPath, String imagePath, rating myRating, String quality,
			boolean subtitles, String language, String writer) {
		super(title, playTime, year, seen, directory, mediaPath, imagePath, myRating);
		this.quality = quality;
		this.subtitles = subtitles;
		this.language = language;
		this.writer = writer;
	}
	
	public Movie() {
		super();
		this.quality = "unknown";
		this.subtitles = false;
		this.language = "unknown";
		this.writer = "unknown";
	}

	public String getQuality() {
		return quality;
	}

	public void setQuality(String quality) {
		this.quality = quality;
	}

	public boolean isSubtitles() {
		return subtitles;
	}

	public void setSubtitles(boolean subtitles) {
		this.subtitles = subtitles;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	// Adds additional integer "0" for identification.
	public String toStringSpec() {
		return 0 + "\r\n" + quality + "\r\n" + subtitles + "\r\n" + language + "\r\n" + writer + "\r\n";
	}
	
	public String infoStringSpec() {
		return "Quality: " + quality + "\r\n" + "Language: " + language + "\r\n" + "Writer: " + writer;
	}
}