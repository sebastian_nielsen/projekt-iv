package project;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.image.BufferedImage;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Vector;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class Shelfs extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private MediaHandler myMediaHandler;
	private Window myWindow;
	
	public Shelfs(Window window) {
	   myWindow = window;
	   setBackground(Color.black);
	   
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		
		setBackground(Color.black);
		//setOpaque(true);
		setBounds(90, 25, 1020, 745);
		new GridLayout(3,6,22,30);
		
		
		int nrOfMedias = myMediaHandler.mediaList.size();
		java.net.URL where;
		for(int i = 0; i < nrOfMedias; i++) {
			String temp = myMediaHandler.mediaList.get(i).getImagePath();
			try {
				where = new URL(temp);
				ImageIcon image = new ImageIcon(where);
				JLabel imageLabel = new JLabel(image);
				//mediaPanel.add(imageLabel);
				this.add(imageLabel);
			} catch (MalformedURLException e) {
				// TODO Auto-generated catch block
				System.out.println("Error loading user media picture");
				e.printStackTrace();
			}
		}

	
	java.net.URL where1;
	try {
		for(int i = nrOfMedias; i < 18; i++) {
			where1 = new URL("https://dl.dropboxusercontent.com/u/16670644/Projekt/TempPic.png");
			ImageIcon image1 = new ImageIcon(where1);			
			JLabel imageLabel1 = new JLabel(image1);
			//mediaPanel.add(imageLabel1);
			this.add(imageLabel1);
		}
		
	} catch (MalformedURLException e) {
		// TODO Auto-generated catch block
		System.out.println("Error loading TempPic that shows allowed media spots");
		e.printStackTrace();
	}
		
	repaint();
		/*for(int i = 0; i < 3; i++) {
			int x = (100 * i);
			
			for(int j = 0; j < 6; j++) {
				int y = (50 * j);
				
				//Shitt will be added
			}
		}*/
	}

}
