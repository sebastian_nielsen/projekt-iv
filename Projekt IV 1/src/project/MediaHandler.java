package project;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import project.Media.rating;

public class MediaHandler {
	ArrayList<Media> mediaList;

	private Window myWindow;

	public MediaHandler(Window window) {
		this.mediaList = new ArrayList<Media>();
		myWindow = window;
	}
	
	public void addMovie(String title, Double playTime, int year,
			String directory, String mediaPath, String imagePath, String quality,
			boolean subtitles, String language, String writer) {
		mediaList.add(new Movie(title, playTime, year, false, directory, mediaPath, imagePath, rating.unrated, quality, subtitles, language, writer));
		
		myWindow.addMedia(imagePath);
	}	
	
	public void addTVShow(String title, Double playTime, int year,
			String directory, String mediaPath, String imagePath, int season,
			int episode) {
		mediaList.add(new TVSeries(title, playTime, year, false, directory, mediaPath, imagePath, rating.unrated, season, episode));
		
		myWindow.addMedia(imagePath);
	}
	
	// Returns the position of the ArrayList by checking then image URL from mediaPanel in Window class against the URL in the ArrayList. 
	public int getPosOfMedia(String imagePath) {
		for(int i = 0; i < mediaList.size(); i++) {
			String actualImagePath = mediaList.get(i).getImagePath();
			if(imagePath.equals(actualImagePath)) {
				return i;
			}
		}
		return -1;
	}
	
	// Returns an Array of a Media Object.
	public String[] getAMediaAsString(int id) {
		String[] s = this.mediaList.get(id).toString().split("\r\n");
		return s;
	}
	
	public Media getMedia(int pos) {
		return this.mediaList.get(pos);
	}
	
	public void saveOnFile() {
		File file = new File("data.txt");
		
		try {
			if(!file.createNewFile()) {
				file.createNewFile();
			}
			FileWriter out = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bW=new BufferedWriter(out);
			
			bW.write(this.mediaList.size() + "\r\n");
			for(int i = 0; i < mediaList.size(); i++) {
				 bW.write(this.mediaList.get(i).toString());
			}
			bW.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
	
	public void readFromFile() {
		FileReader fileReider;
		try {
			fileReider = new FileReader("data.txt");
			BufferedReader bufferedReader = new BufferedReader(fileReider);
			int tempSize = Integer.parseInt(bufferedReader.readLine());
			
			for(int i = 0; i < tempSize; i++) {
				// Get items from file
				String title = bufferedReader.readLine();
				Double playTime = Double.parseDouble(bufferedReader.readLine());
				int year = Integer.parseInt(bufferedReader.readLine());
				bufferedReader.readLine();
				String directory = bufferedReader.readLine();
				String mediaPath = bufferedReader.readLine();
				String imagePath = bufferedReader.readLine();
				bufferedReader.readLine();
				int objectTypeValue = Integer.parseInt(bufferedReader.readLine());
				
				if(objectTypeValue == 0) { // If Movie add movie else add TV-show
					String quality = bufferedReader.readLine();
					boolean subtitles = Boolean.parseBoolean(bufferedReader.readLine());
					String language = bufferedReader.readLine();
					String writer = bufferedReader.readLine();
					
					addMovie(title, playTime, year, directory, mediaPath, imagePath, quality, subtitles, language, writer);
				} else {
					int season = Integer.parseInt(bufferedReader.readLine());
					int episode = Integer.parseInt(bufferedReader.readLine());
					
					addTVShow(title, playTime, year, directory, mediaPath, imagePath, season, episode);
				}
			}
			bufferedReader.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
}