package project;


import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;



public class AddTVShowWindow extends JFrame {
	
	private class ButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String buttonAction = e.getActionCommand();
			switch(buttonAction) {
			case "Add":
				addMedia();
				dispose();
				break;
			}
		}
	}
	

	private static final long serialVersionUID = 1L;
	
	private JTextField title;	
	private JTextField playTime;
	private JTextField year;
	private JTextField directory;
	private JTextField mediaPath; 
	private JTextField imagePath;
	private JTextField season;
	private JTextField episode;
	private MediaHandler myMediaHandler;
	private Container contentPanel;
	
	public AddTVShowWindow(MediaHandler myMediaHandler) {
		this.myMediaHandler = myMediaHandler;
		initiateinstancevariable();
		configurFrame();
		addComponents();
	}
	
	private void addMedia() {
		String title1 = this.title.getText();
		
		double playTime1 = 0;
		try {
			playTime1 = Double.parseDouble(this.playTime.getText());
		} catch(NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "Playtime is not a valid number!" , "ERROR", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		int year1 = 0;
		try {
			year1 = Integer.parseInt(this.year.getText());
		} catch(NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "Year is not a valid number!" , "ERROR", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		String directory1 = this.directory.getText();
		String mediaPath1 = this.mediaPath.getText();
		String imagePath1 = this.imagePath.getText();
		
		
		int season1 = 00;
		try {
			season1 = Integer.parseInt(this.season.getText());
		} catch(NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "Season is not a valid number!" , "ERROR", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		
		int episode1 = 00;
		
		try {
			episode1 = Integer.parseInt(this.episode.getText());
		} catch(NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "Episode is not a valid number!" , "ERROR", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		try {
			this.myMediaHandler.addTVShow(title1, playTime1, year1, directory1, mediaPath1, imagePath1, season1, episode1);
		} catch(Exception e) {
			JOptionPane.showMessageDialog(this, "Could not add the TV-Show!" , "ERROR", JOptionPane.ERROR_MESSAGE);
			return;
		}
	}
	
	private void configurFrame() {
		this.setSize(400, 600);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setTitle("Add TV-Show");
		this.setIconImage(new ImageIcon("icon.png").getImage());
	}
	
	private void addComponents() {
		this.contentPanel.add(this.title);
		this.contentPanel.add(this.playTime);
		this.contentPanel.add(this.year);
		this.contentPanel.add(this.directory);
		this.contentPanel.add(this.mediaPath);
		this.contentPanel.add(this.imagePath);
		this.contentPanel.add(this.season);
		this.contentPanel.add(this.episode);
		
		JButton button = new JButton("Add");
		button.setBounds(250, 500, 100, 40);
		ButtonListener buttonList = new ButtonListener();
		button.addActionListener(buttonList);
		
		this.contentPanel.add(button);
	}

	private void initiateinstancevariable() {
		this.title = new JTextField();
		this.title.setBounds(20, 20, 200, 40);
		this.title.setBorder(BorderFactory.createTitledBorder("Title"));

		this.playTime = new JTextField();
		this.playTime.setBounds(20,70,200,40);
		this.playTime.setBorder(BorderFactory.createTitledBorder("Playtime"));
		
		this.year = new JTextField();
		this.year.setBounds(20, 120, 200, 40);
		this.year.setBorder(BorderFactory.createTitledBorder("Year"));
		
		this.directory = new JTextField();
		this.directory.setBounds(20,170,200,40);
		this.directory.setBorder(BorderFactory.createTitledBorder("Directory"));
		
		this.mediaPath = new JTextField();
		this.mediaPath.setBounds(20, 220, 200, 40);
		this.mediaPath.setBorder(BorderFactory.createTitledBorder("Media Path"));
		
		this.imagePath = new JTextField();
		this.imagePath.setBounds(20, 270, 200, 40);
		this.imagePath.setBorder(BorderFactory.createTitledBorder("Image path"));
		
		this.season = new JTextField();
		this.season.setBounds(20, 320, 200, 40);
		this.season.setBorder(BorderFactory.createTitledBorder("Season"));
		
		this.episode = new JTextField();
		this.episode.setBounds(20, 370, 200, 40);
		this.episode.setBorder(BorderFactory.createTitledBorder("Episode"));
		
		this.contentPanel = this.getContentPane();
		this.contentPanel.setLayout(null);
		
	}

}
