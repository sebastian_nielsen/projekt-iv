package project;

public class TVSeries extends Media {

	private int season;
	private int episode;
	
	public TVSeries(String title, Double playTime, int year, boolean seen,
			String directory, String mediaPath, String imagePath, rating myRating, int season,
			int episode) {
		super(title, playTime, year, seen, directory, mediaPath, imagePath, myRating);
		this.season = season;
		this.episode = episode;
	}

	public TVSeries() {
		super();
		this.season = -1;
		this.episode = -1;
	}

	public int getSeason() {
		return season;
	}

	public void setSeason(int season) {
		this.season = season;
	}

	public int getSpisode() {
		return episode;
	}

	public void setSpisode(int episode) {
		this.episode = episode;
	}

	// Adds additional integer "1" for identification.
	public String toStringSpec() {
		return 1 + "\r\n" + season  + "\r\n" + episode + "\r\n";
	}
	
	public String infoStringSpec() {
		return "Season: " + season + "\r\n" + "Episode: " + episode;
	}
	



}