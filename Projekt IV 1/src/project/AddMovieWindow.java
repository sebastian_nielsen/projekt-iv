package project;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class AddMovieWindow extends JFrame {
	
	private class ButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String buttonAction = e.getActionCommand();
			switch(buttonAction) {
			case "Add":
				addMedia();
				dispose();
				break;
			}			
		}
	}
	
	private static final long serialVersionUID = 1L;
	
	private JTextField title;	
	private JTextField playTime;
	private JTextField year;
	private JTextField directory;
	private JTextField quality;
	private JTextField mediaPath; 
	private JTextField imagePath;
	private JCheckBox subtitles;
	private JTextField language;
	private JTextField writer;
	private MediaHandler myMediaHandler;
	private Container contentPanel;

	public AddMovieWindow(MediaHandler myMediaHandler) {
		this.myMediaHandler = myMediaHandler;
		initiateinstancevariable();
		configurFrame();
		addComponents();
	}
	
	public void addMedia() {
		String title1 = this.title.getText();
		
		double playTime1 = 0;
		try {
			playTime1 = Double.parseDouble(this.playTime.getText());
		} catch(NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "Playtime is not a valid number!" , "ERROR", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		int year1 = 0;
		try {
			year1 = Integer.parseInt(this.year.getText());
		} catch(NumberFormatException e) {
			JOptionPane.showMessageDialog(this, "Year is not a valid number!" , "ERROR", JOptionPane.ERROR_MESSAGE);
			return;
		}
		
		String directory1 = this.directory.getText();
		
		String mediaPath1 = this.mediaPath.getText();
		String imagePath1 = this.imagePath.getText();
		String quality1 = this.quality.getText();
		
		boolean subtitles1 = false;
		if(this.subtitles.isSelected()) {
			subtitles1 = true;
		}
		
		String language1 = this.language.getText();
		String writer1 = this.writer.getText();
		
		try {
			this.myMediaHandler.addMovie(title1, playTime1, year1, directory1, mediaPath1, imagePath1, quality1, subtitles1, language1, writer1);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, "Could not add the movie!" , "ERROR", JOptionPane.ERROR_MESSAGE);
			return;
		}
	}
	
	private void configurFrame() {
		this.setSize(400, 600);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setTitle("Add Movie");
		this.setIconImage(new ImageIcon("icon.png").getImage());
	}
	
	private void addComponents() {
		this.contentPanel.add(this.title);
		this.contentPanel.add(this.playTime);
		this.contentPanel.add(this.year);
		this.contentPanel.add(this.directory);
		this.contentPanel.add(this.quality);
		this.contentPanel.add(this.mediaPath);
		this.contentPanel.add(this.imagePath);
		this.contentPanel.add(this.language);
		this.contentPanel.add(this.writer);
		this.contentPanel.add(this.subtitles);
		
		JButton button = new JButton("Add");
		button.setBounds(250, 500, 100, 40);
		ButtonListener buttonList = new ButtonListener();
		button.addActionListener(buttonList);
		
		this.contentPanel.add(button);
	}
	
	private void initiateinstancevariable() {
		this.title = new JTextField();
		this.title.setBounds(20, 20, 200, 40);
		this.title.setBorder(BorderFactory.createTitledBorder("Title"));

		this.playTime = new JTextField();
		this.playTime.setBounds(20,70,200,40);
		this.playTime.setBorder(BorderFactory.createTitledBorder("Playtime"));
		
		this.year = new JTextField();
		this.year.setBounds(20, 120, 200, 40);
		this.year.setBorder(BorderFactory.createTitledBorder("Year"));
		
		this.directory = new JTextField();
		this.directory.setBounds(20,170,200,40);
		this.directory.setBorder(BorderFactory.createTitledBorder("Directory"));
		
		this.quality = new JTextField();
		this.quality.setBounds(20, 220, 200, 40);
		this.quality.setBorder(BorderFactory.createTitledBorder("Quality"));
		
		this.mediaPath = new JTextField();
		this.mediaPath.setBounds(20, 270, 200, 40);
		this.mediaPath.setBorder(BorderFactory.createTitledBorder("Media Path"));
		
		this.imagePath = new JTextField();
		this.imagePath.setBounds(20, 320, 200, 40);
		this.imagePath.setBorder(BorderFactory.createTitledBorder("Image path"));
		
		this.language = new JTextField();
		this.language.setBounds(20, 370, 200, 40);
		this.language.setBorder(BorderFactory.createTitledBorder("Language"));
		
		this.writer = new JTextField();
		this.writer.setBounds(20, 420, 200, 40);
		this.writer.setBorder(BorderFactory.createTitledBorder("Writer"));
		
		this.subtitles = new JCheckBox();
		this.subtitles.setBounds(20, 470, 200, 30);
		this.subtitles.setText("Subtitles");
		
		this.contentPanel = this.getContentPane();
		this.contentPanel.setLayout(null);
		
	}
}
