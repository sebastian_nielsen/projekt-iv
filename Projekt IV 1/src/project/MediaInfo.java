package project;

import java.awt.Container;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

public class MediaInfo extends JFrame {
	
	private Media aMedia;
	
	private class ButtonListener implements ActionListener {

		@Override
		public void actionPerformed(ActionEvent e) {
			String buttonAction = e.getActionCommand();
			switch(buttonAction) {
			case "Play":
				playMedia();
				dispose();
				break;
			}
		}
	}

	private static final long serialVersionUID = 1L;
	private JList<String> mediaList;
	private Container contentPanel;
	
	// Takes the media sent from Window class and adds it to the JList. infoString is used to only display the information i want. 
	public MediaInfo(Media aMedia) {
		initiateinstancevariable();
		configureFrame();
		addComponents();

		this.mediaList.setListData(aMedia.infoString().split("\r\n"));
		this.aMedia=aMedia;
	}

	private void configureFrame() {
		this.setSize(500, 300);
		this.setResizable(false);
		this.setLocationRelativeTo(null);
		this.setTitle("Media info");
		this.setIconImage(new ImageIcon("icon.png").getImage());
		this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
	}
	
	private void addComponents() {
		JButton button = new JButton("Play");
		button.setBounds(25, 200, 100, 50);
		ButtonListener buttonList = new ButtonListener();
		button.addActionListener(buttonList); 
		
		this.contentPanel.add(button);
		this.contentPanel.add(this.mediaList);
	}
	
	private void initiateinstancevariable() {
		this.mediaList = new JList<>();
		this.mediaList.setBounds(150, 0, 350, 300);
		this.mediaList.setBorder(BorderFactory.createTitledBorder("Media info"));

		this.contentPanel = this.getContentPane();
		this.contentPanel.setLayout(null);
	}
	
	// Opens the media selected by sending it to OS by the getDesktop() function. This allows the operating system to open the file with its standard playing method.
	// This way there wont be any problem playing the media. The default program will run it given that the file is found. 
	public void playMedia() {
		try {
			Desktop.getDesktop().open(new File(this.aMedia.getMediaPath()));
		} catch (IllegalArgumentException ex) {
			JOptionPane.showMessageDialog(this, "File not found!" , "ERROR", JOptionPane.ERROR_MESSAGE);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}